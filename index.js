const util = require('util');
const {NFC, NFCReader, NFC_PROPERTY} = require('libnfc-js');
const Promise = require('bluebird');
const debug = require('debug')('wne');
const gm = require('gm').subClass({imageMagick: true});

const TAG_ID = Buffer.from('WSDZ10m', 'ascii');
const SCREEN = {WIDTH: 0, HEIGHT: 0, BITS: 0, LOOPS: 0, ID: 0,}

const nfc = new NFC();
const devices = nfc.listDevices();
nfc.close();

if (devices.length === 0) {
  console.log('No NFC devices found, exiting');
  process.exit(1);
}

const nfcReader = new NFCReader();
nfcReader.open();

async function main() {
  let whichWs = process.argv[2];
  if (whichWs == '4.2') {
    SCREEN.WIDTH = 400;
    SCREEN.HEIGHT = 300;
    SCREEN.BITS = 100;
    SCREEN.LOOPS = 150;
    SCREEN.ID = 10;
  } else if (whichWs == '7.5') {
    SCREEN.WIDTH = 800;
    SCREEN.HEIGHT = 480;
    SCREEN.BITS = 120;
    SCREEN.LOOPS = 400;
    SCREEN.ID = 14;
  } else {
    console.log('Expecting screen size as first parameter (4.2 or 7.5), exiting');
    process.exit(1);
  }
  const photoData = await parseImage(process.argv[3]);
  nfcReader.poll();
  nfcReader.on('card', canSendPic(photoData));
}

async function transceive(c) {
  const command = Buffer.from(c);
  debug({command});
  try {
    const result = await nfcReader.transceive(command);
    if (
      command[1] == 0xcd &&
      (result.length !== 2 || result[0] !== 0x00 || result[1] !== 0x00)
    ) {
      throw `non-zero response ${result}`;
    }
    return result;
  } catch (e) {
    console.log('transceive error', JSON.stringify(e));
    throw e;
  }
}

async function parseImage(photo) {
  const filepath = photo || 'photo.png';
  let original = gm(filepath);
  const size = util.promisify(original.size.bind(original));
  const {width, height} = await size();
  if (height > width) {
    original = original.rotate('white', 270);
  }
  original = original
    .gravity('Center')
    .resize(SCREEN.WIDTH, SCREEN.HEIGHT)
    .extent(SCREEN.WIDTH, SCREEN.HEIGHT)
    .colorspace('GRAY')
    .type('Bilevel')
    .bitdepth(2)
    .negative();

  return new Promise((resolve, reject) => {
    original.toBuffer('pbm', (err, buffer) => {
      // 'pbm': http://www.graphicsmagick.org/GraphicsMagick.html
      if (err) {
        reject(err);
      }
      resolve(buffer.slice(11));
    });
  });
}

function canSendPic(photoData) {
  return async card => {
    const line_prefix = Buffer.from([0xcd, 0x8, SCREEN.BITS]);
    const line_width = SCREEN.BITS;

    const uid = await transceive([0x30, 0x00]);
    if (!TAG_ID.equals(uid.slice(0, 7))) {
      console.log('Wrong UID, exiting');
      process.exit(1);
    }

    nfcReader.setProperty(NFC_PROPERTY.NP_EASY_FRAMING, false);

    let success = false;
    do {
      try {
        await tagSetup();

        for (var i = 0; i < SCREEN.LOOPS; i++) {
          await sleep(20);
          const line = photoData.slice(
            i * line_width,
            i * line_width + line_width,
          );
          await transceive(Buffer.concat([line_prefix, line]));
        }

        await tagFinal();
        success = true;
      } catch (err) {
        success = false;
        await sleep(500);
      }
    } while (!success);

    // await nfcReader.release();
  };
}

async function tagSetup() {
  const commands = [
    [0xcd, 0xd],
    [0xcd, 0x0, SCREEN.ID],
    [0xcd, 0x1],
    [0xcd, 0x2],
    [0xcd, 0x3],
    [0xcd, 0x5],
    [0xcd, 0x6],
    [0xcd, 0x7, 0x0],
  ];
  await Promise.mapSeries(commands, async command => {
    await sleep(100);
    await transceive(command);
  });
}

async function tagFinal() {
  const commands = [
    [0xcd, 0x9], // 0x09 is refresh
    [0xcd, 0xa],
    [0xcd, 0x4],
  ];

  await sleep(200);
  await Promise.mapSeries(commands, async command => {
    await transceive(command);
  });
}

function sleep(ms) {
  return new Promise(resolve => setTimeout(resolve, ms));
}

main();
